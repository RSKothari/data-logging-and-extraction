#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 19:30:25 2018

@author: rakshit
"""

import pyzed.camera as zcam
import pyzed.types as tp
import pyzed.core as core
import pyzed.defines as sl
import os
import cv2
import scipy.io as sio
global PATH_TO_DATASET, PATH_TO_SVO
import numpy as np

if __name__ == "__main__":
        
    PATH_TO_DATASET = '/run/user/1000/gvfs/smb-share:server=mvrlsmb.cis.rit.edu,share=performlab/Natural statistics/'
    NAME = input('Enter Name: ')
    TRIAL = input('Enter Trial No: ')
    PATH_TO_FOLDER = PATH_TO_DATASET + '/' + NAME + '/' + str(TRIAL) + '/'
    PATH_TO_SVO =  PATH_TO_FOLDER + 'video.svo'
    fid = open(PATH_TO_FOLDER + 'PosData.txt', 'w')
    fid.write('Timestamp, tx, ty, tz, ow, ox, oy, oz\n')
    
    if not os.path.exists(PATH_TO_FOLDER + 'Depth'):
        os.makedirs(PATH_TO_FOLDER + 'Depth')
    if not os.path.exists(PATH_TO_FOLDER + 'RGB-Left'):
        os.makedirs(PATH_TO_FOLDER + 'RGB-Left')
    if not os.path.exists(PATH_TO_FOLDER + 'RGB-Right'):
        os.makedirs(PATH_TO_FOLDER + 'RGB-Right')
    if not os.path.exists(PATH_TO_FOLDER + 'PC'):
        os.makedirs(PATH_TO_FOLDER + 'PC')
    
    init = zcam.PyInitParameters(svo_input_filename=PATH_TO_SVO, svo_real_time_mode = False)
    init.coordinate_system = sl.PyCOORDINATE_SYSTEM.PyCOORDINATE_SYSTEM_RIGHT_HANDED_Y_UP
    init.coordinate_units = sl.PyUNIT.PyUNIT_MILLIMETER
    
    cam = zcam.PyZEDCamera()
    status = cam.open(init)
    
    if status != tp.PyERROR_CODE.PySUCCESS:
        print(repr(status))
        exit()
    
    
    print("Resolution: {0}, {1}.".format(round(cam.get_resolution().width, 2), cam.get_resolution().height))
    print("Camera FPS: {0}".format(cam.get_camera_fps()))
    print("Frame count: {0}.\n".format(cam.get_svo_number_of_frames()))
    
    XForm = core.PyTransform()
    tracking_parameters = zcam.PyTrackingParameters(init_pos = XForm)
    status = cam.enable_tracking(tracking_parameters)
    if status != tp.PyERROR_CODE.PySUCCESS:
        print('Could not set tracking parameters')
        exit(1)
    
    runtime = zcam.PyRuntimeParameters()
    mat_left = core.PyMat()
    mat_right = core.PyMat()
    #mat_depth = core.PyMat()
    zed_pose = zcam.PyPose()
    point_cloud = core.PyMat()
    
    extract_frames = True
    count = 0
    
    while extract_frames:
        err = cam.grab(runtime)
        if err == tp.PyERROR_CODE.PySUCCESS:
            TS = cam.get_timestamp(sl.PyTIME_REFERENCE.PyTIME_REFERENCE_IMAGE)
            cam.retrieve_image(mat_left, sl.PyVIEW.PyVIEW_LEFT)
            cam.retrieve_image(mat_right, sl.PyVIEW.PyVIEW_RIGHT)
            #cam.retrieve_measure(mat_depth, sl.PyMEASURE.PyMEASURE_DEPTH)
            cam.retrieve_measure(point_cloud, sl.PyMEASURE.PyMEASURE_XYZ)
            
            cv2.imwrite(PATH_TO_FOLDER + 'RGB-Left/' + str(count) + '.jpg', mat_left.get_data())
            cv2.imwrite(PATH_TO_FOLDER + 'RGB-Right/' + str(count) + '.jpg', mat_right.get_data())
            #cv2.imwrite(PATH_TO_FOLDER + 'Depth/' + str(count) + '.jpg', mat_depth.get_data())
            #sio.savemat(PATH_TO_FOLDER + 'Depth/' + str(count) + '.mat', {'Depth': mat_depth.get_data()}, do_compression=True)
            sio.savemat(PATH_TO_FOLDER + 'PC/' + str(count) + '.mat', {'PC': point_cloud.get_data()}, do_compression=True)
            
            cam.get_position(zed_pose, sl.PyREFERENCE_FRAME.PyREFERENCE_FRAME_CAMERA)
            
            py_translation = core.PyTranslation()
            tx = round(zed_pose.get_translation(py_translation).get()[0], 4)
            ty = round(zed_pose.get_translation(py_translation).get()[1], 4)
            tz = round(zed_pose.get_translation(py_translation).get()[2], 4)
            fid.write("{3}, {0}, {1}, {2}, ".format(tx, ty, tz, TS))

            # Display the orientation quaternion
            py_orientation = core.PyOrientation()
            ox = round(zed_pose.get_orientation(py_orientation).get()[0], 6)
            oy = round(zed_pose.get_orientation(py_orientation).get()[1], 6)
            oz = round(zed_pose.get_orientation(py_orientation).get()[2], 6)
            ow = round(zed_pose.get_orientation(py_orientation).get()[3], 6)
            fid.write("{3}, {0}, {1}, {2}\n".format(ox, oy, oz, ow))
            
            count += 1
            print('Frame no: {:}'.format(count))
        elif err != tp.PyERROR_CODE.PySUCCESS or count == cam.get_svo_number_of_frames:
            print("Extracted all frames")
            extract_frames = False
    
