#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 19:30:24 2018

@author: rakshit
"""

import cv2
import threading
import os
import logging
import numpy as np

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )

class WriteVideo:
    def __init__(self, VidObj, PathToWrite):
        self.VidObj = VidObj
        self.PathToWrite = PathToWrite
        self.count = 0
        if not os.path.exists(PathToWrite):
            os.makedirs(PathToWrite)
        
    def write(self):
        logging.debug('Started')
        self.count = 0
        KeepRun = True
        while(self.VidObj.isOpened() and KeepRun):
            ret, frame = self.VidObj.read()
            
            if ret is False:
                KeepRun = False
            else:
                cv2.imwrite(self.PathToWrite + str(self.count) + '.jpg', frame)
                self.count += 1
                
        logging.debug('Finished')
        self.VidObj.release()
    

def writeToFile(T, str_file):
    fid = open(str_file, 'w')
    for i in range(0, len(T)):
        fid.write('{}, {}\n'.format(i, T[i]))
    fid.close()
    return None

global PATH_TO_DATASET
if __name__ == '__main__':
    
    PATH_TO_DATASET = '/run/user/1000/gvfs/smb-share:server=mvrlsmb.cis.rit.edu,share=performlab/Natural statistics/'
    NAME = input('Enter Name: ')
    TRIAL = input('Enter Trial No: ')
    PATH_TO_GAZE = PATH_TO_DATASET + NAME + '/' + str(TRIAL) + '/Gaze/'
    
    Eye0_vid = cv2.VideoCapture(PATH_TO_GAZE + 'eye0.mp4')
    Eye1_vid = cv2.VideoCapture(PATH_TO_GAZE + 'eye1.mp4')
    Scene_vid = cv2.VideoCapture(PATH_TO_GAZE + 'world.mp4')
    
    Eye0_Writer = WriteVideo(Eye0_vid, PATH_TO_GAZE + 'Eye0/')
    Eye1_Writer = WriteVideo(Eye1_vid, PATH_TO_GAZE + 'Eye1/')
    Scene_Writer = WriteVideo(Scene_vid, PATH_TO_GAZE + 'Scene/')
    
    writeToFile(np.load(PATH_TO_GAZE + 'eye0_timestamps.npy'), PATH_TO_GAZE + 'Eye0/timing.txt')
    writeToFile(np.load(PATH_TO_GAZE + 'eye1_timestamps.npy'), PATH_TO_GAZE + 'Eye1/timing.txt')
    writeToFile(np.load(PATH_TO_GAZE + 'world_timestamps.npy'), PATH_TO_GAZE + 'Scene/timing.txt')
    
    func_1 = threading.Thread(name='Eye0 write', target=Eye0_Writer.write, args=())
    func_2 = threading.Thread(name='Eye1 write', target=Eye1_Writer.write, args=())
    func_3 = threading.Thread(name='Scene_write', target=Scene_Writer.write, args=())
    
    func_1.start()
    func_2.start()
    func_3.start()
