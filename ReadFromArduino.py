from __future__ import division
from datetime import datetime
from parse import parse
import serial
import threading
import time
import os

class ReadData:
    def __init__(self, fileID):
        self.FObj = fileID
        
    def run(self, arduino):
        global writeToFile, KeepRun, calibNow, DataAvailable
        arduino.flush()
        arduino.write('S')
        time.sleep(3)
	print 'Finished sleep'
        
        while not DataAvailable:
            data = arduino.readline()
            if 'start' in data.lower():
                DataAvailable = True

        while KeepRun:
            str_write = self.readFromArduino(arduino)
            
            if writeToFile and str_write:
                self.FObj.write(str_write)
        
        self.FObj.close()
        arduino.close()
        
    def readFromArduino(self, arduino):
        global calibNow
        
        data = arduino.readline()
        data = parse('n: {:d},w: {:f},x: {:f},y: {:f},z: {:f}\r\n', data)
        dt = datetime.now()
        if data:
            str_write = '{:d}, {:d}, {:d}, {:d}, {:d}, {:.8f}, {:.8f}, {:.8f}, {:.8f}\n'.format(\
                int(calibNow), dt.hour, dt.minute, dt.second, dt.microsecond,\
                float(data[1]), float(data[2]), float(data[3]), float(data[4]))
            return str_write
        else:
            return False
        
        
    def getTime(self):
        dt = datetime.now()
        return 60*60*dt.hour + 60*dt.minute + dt.second + pow(10,-6) * dt.microsecond

class listener:
    def __init__(self):
        self.Key = 0
    def run(self):
        global writeToFile, KeepRun, calibNow
        
        while KeepRun:
            self.Key = raw_input('Enter command: ').upper()
            
            if self.Key == 'W':
                writeToFile = True
                calibNow = False
            if self.Key == 'Q':
                writeToFile = False
                KeepRun = False
            if self.Key == 'C':
                calibNow = True
                writeToFile = True

if __name__=='__main__':
	global writeToFile, KeepRun, calibNow, DataAvailable
	
	PATH_TO_DATA = '/home/rakshit/Dataset/'
	NAME = raw_input("Name: ")
	TRIAL = raw_input("Trial No: ")
	IMU_FILE_PATH = PATH_TO_DATA + '/' + NAME + '/' + TRIAL + '/' + 'IMU' + '/'
	
	if not os.path.exists(IMU_FILE_PATH):
            os.makedirs(IMU_FILE_PATH)
	
	myfile = open(IMU_FILE_PATH + 'IMUData.txt', 'w')
        myfile.write('Calib, Hour, Minute, Second, Microsecond, Qw, Qx, Qy, Qz\n')
	
	calibNow = False
	writeToFile = False
	KeepRun = True
	DataAvailable = False

	arduino = serial.Serial('/dev/ttyACM0', 115200)
        time.sleep(3)
	print 'Finished first sleep'
        
	p = ReadData(myfile)
	q = listener()

	func_1 = threading.Thread(target = p.run, args = (arduino, ))
	func_2 = threading.Thread(target = q.run, args= ())

	func_1.start()
	func_2.start()
