import threading
from datetime import datetime

import pyzed.camera as zcam
import pyzed.core as core
import pyzed.defines as sl
import pyzed.types as tp
import time
import os

global CAM
global KEEPRUN
global RECORD_VIDEO_FLAG
global runtime
global fid
global fid_stats

def print_camera_information(cam):
    print("Resolution: {0}, {1}.".format(round(cam.get_resolution().width, 2), cam.get_resolution().height))
    print("Camera FPS: {0}.".format(cam.get_camera_fps()))
    print("Firmware: {0}.".format(cam.get_camera_information().firmware_version))
    print("Serial number: {0}.\n".format(cam.get_camera_information().serial_number))

def main(svo_filepath, time_filepath):
    init = zcam.PyInitParameters(camera_resolution=sl.PyRESOLUTION.PyRESOLUTION_HD1080)

    global CAM
    global runtime
    global fid
    global fid_stats

    CAM = zcam.PyZEDCamera()
    status = CAM.open(init)
    fid = open(time_filepath, 'w')
    fid_stats = open('Stats.txt', 'w')

    if status != tp.PyERROR_CODE.PySUCCESS:
        print(repr(status))
        exit()

    if RECORD_VIDEO_FLAG is True:
        vid = CAM.enable_recording(svo_filepath, sl.PySVO_COMPRESSION_MODE.PySVO_COMPRESSION_MODE_LOSSY)
        print(repr(vid))

    runtime = zcam.PyRuntimeParameters()
    runtime.enable_depth = False

    print_camera_information(CAM)

    start_zed()

def start_zed():

    global RECORD_VIDEO_FLAG
    global CAM

    print("Begin recording")
    listener_obj = listener()
    listener_callback = threading.Thread(target=listener_obj.run, args=())
    listener_callback.start()

    if RECORD_VIDEO_FLAG is True:
        recorder_obj = record_video()
        recorder_callback = threading.Thread(target=recorder_obj.run, args=())
        recorder_callback.start()

class record_video:
    def run(self):
        global KEEPRUN
        global CAM
        global runtime
        global fid
        global fid_stats
        count = 0
        
        while KEEPRUN:
            #startT = time.time()
            err = CAM.grab(runtime)
            #endT = time.time()
            #fid_stats.write('{}, '.format(endT - startT))
            if err == tp.PyERROR_CODE.PySUCCESS:
                fid.write(("{}, " + self.getTime() + "\n").format(count))
                #startT = time.time()                
                CAM.record()
                #endT = time.time()
                #fid_stats.write('{}\n'.format(endT - startT))
                count = count + 1

        fid.close()
        fid_stats.close()
        
        print("Disabling video record ..")
        print("Total frames recorded: {}".format(count))
        CAM.disable_recording()
        print("Recording finished")
        CAM.close()

    def getTime(self):
        dt = datetime.now()
        time_str = "{}, {}, {}, {}".format(dt.hour, dt.minute, dt.second, dt.microsecond)
        return time_str

class listener:
    def __init__(self):
        self.KEY = 0

    def run(self):
        global KEEPRUN
        KEEPRUN = True
        while KEEPRUN:
            self.KEY = input('Enter command: ').upper()

            if self.KEY == 'Q':
                KEEPRUN = False

if __name__ == "__main__":

    CAM = []
    RECORD_VIDEO_FLAG = True
    PATH_TO_DATA = '/home/rakshit/Dataset/'
    NAME = input("Name: ")
    TRIAL = input("Trial no: ")
    AGE = input("Age: ")
    SVO_FILEPATH = PATH_TO_DATA + NAME + '/' + str(TRIAL) + '/'
    TIME_FILEPATH = PATH_TO_DATA + NAME + '/' + str(TRIAL) + '/' + 'timing.txt'

    if not os.path.exists(SVO_FILEPATH):
        os.makedirs(SVO_FILEPATH)
        os.makedirs(SVO_FILEPATH + 'Depth')
        os.makedirs(SVO_FILEPATH + 'RGB-Left')
        os.makedirs(SVO_FILEPATH + 'RGB-Right')

    with open(PATH_TO_DATA + "ParticipantData.txt", "a") as myfile:
        myfile.write("\n{}, {}".format(NAME, AGE))

    main(SVO_FILEPATH  + 'video.svo', TIME_FILEPATH)
