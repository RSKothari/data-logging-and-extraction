#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 19:30:25 2018

@author: rakshit
"""

import pyzed.camera as zcam
import pyzed.types as tp
import pyzed.core as core
import pyzed.defines as sl
import os
import cv2
import scipy.io as sio
global PATH_TO_DATASET, PATH_TO_SVO
import numpy as np

if __name__ == "__main__":
        
    PATH_TO_DATASET = '/home/rakshit/Documents/MATLAB/event_detection_mark_1/CameraCalibration/CalibrationFiles/'
    PATH_TO_FOLDER = PATH_TO_DATASET
    PATH_TO_SVO =  PATH_TO_DATASET + 'StereoCalibration.svo'
    
    if not os.path.exists(PATH_TO_FOLDER + 'RGB-Left'):
        os.makedirs(PATH_TO_FOLDER + 'RGB-Left')
    if not os.path.exists(PATH_TO_FOLDER + 'RGB-Right'):
        os.makedirs(PATH_TO_FOLDER + 'RGB-Right')

    init = zcam.PyInitParameters(svo_input_filename=PATH_TO_SVO, svo_real_time_mode = False)
    init.coordinate_system = sl.PyCOORDINATE_SYSTEM.PyCOORDINATE_SYSTEM_RIGHT_HANDED_Y_UP
    init.coordinate_units = sl.PyUNIT.PyUNIT_MILLIMETER
    
    cam = zcam.PyZEDCamera()
    status = cam.open(init)
    
    if status != tp.PyERROR_CODE.PySUCCESS:
        print(repr(status))
        exit()
    
    
    print("Resolution: {0}, {1}.".format(round(cam.get_resolution().width, 2), cam.get_resolution().height))
    print("Camera FPS: {0}".format(cam.get_camera_fps()))
    print("Frame count: {0}.\n".format(cam.get_svo_number_of_frames()))
    
    XForm = core.PyTransform()
    tracking_parameters = zcam.PyTrackingParameters(init_pos = XForm)
    status = cam.enable_tracking(tracking_parameters)
    if status != tp.PyERROR_CODE.PySUCCESS:
        print('Could not set tracking parameters')
        exit(1)
    
    runtime = zcam.PyRuntimeParameters()
    mat_left = core.PyMat()
    mat_right = core.PyMat()
    
    extract_frames = True
    count = 0
    
    while extract_frames:
        err = cam.grab(runtime)
        if err == tp.PyERROR_CODE.PySUCCESS:
            cam.retrieve_image(mat_left, sl.PyVIEW.PyVIEW_LEFT)
            cam.retrieve_image(mat_right, sl.PyVIEW.PyVIEW_RIGHT)
            
            cv2.imwrite(PATH_TO_FOLDER + 'RGB-Left/' + str(count) + '.jpg', mat_left.get_data())
            cv2.imwrite(PATH_TO_FOLDER + 'RGB-Right/' + str(count) + '.jpg', mat_right.get_data())
            
            count += 1
            print('Frame no: {:}'.format(count))
        elif err != tp.PyERROR_CODE.PySUCCESS or count == cam.get_svo_number_of_frames:
            print("Extracted all frames")
            extract_frames = False
    
