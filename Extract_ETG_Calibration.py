#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 19:30:24 2018

@author: rakshit
"""

import cv2
import threading
import os
import logging
import numpy as np

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )

class WriteVideo:
    def __init__(self, VidObj, PathToWrite):
        self.VidObj = VidObj
        self.PathToWrite = PathToWrite
        self.count = 0
        if not os.path.exists(PathToWrite):
            os.makedirs(PathToWrite)
        
    def write(self):
        logging.debug('Started')
        self.count = 0
        KeepRun = True
        while(self.VidObj.isOpened() and KeepRun):
            ret, frame = self.VidObj.read()
            
            if ret is False:
                KeepRun = False
            else:
                cv2.imwrite(self.PathToWrite + str(self.count) + '.jpg', frame)
                self.count += 1
                
        logging.debug('Finished')
        self.VidObj.release()

global PATH_TO_DATASET
if __name__ == '__main__':
    
    PATH_TO_DATASET = '/home/rakshit/recordings/2018_03_24/000/'
    Scene_vid = cv2.VideoCapture(PATH_TO_DATASET + 'world.mp4')
    Scene_Writer = WriteVideo(Scene_vid, PATH_TO_DATASET + 'Scene/')
    
    func_3 = threading.Thread(name='Scene_write', target=Scene_Writer.write, args=())
    
    func_3.start()
